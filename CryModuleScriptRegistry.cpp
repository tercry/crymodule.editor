#include "Material.h"

namespace Editor
{
	class ScriptRegistry : 
		public CryModule::Script::IFactory,
		public CryModule::Class<ScriptRegistry>
	{
	public:

		CryModule::Script::SFactory *GetFactoryItems(size_t &nCount)
		{

			static CryModule::Script::SFactory aItems[] = 
			{
				CryModule::Script::SFactory(CMaterial::GetObjectThunk(), "Editor.Material", "Matertial Editor Scripting Interface"),	
			};

			nCount = sizeof(aItems) / sizeof(aItems[0]);

			return &aItems[0];
		}
	} UtilsPluginClassRegister(ScriptRegistry);
}