#pragma once

#include <CryModule/Class.h>
#include <ISystem.h>
#include <IStatObj.h>
#include <IRenderer.h>
#include <_ScriptObject.h>

struct IMatInfo;

namespace Editor
{
	class CMaterial : 
#if 0
		public _ScriptObject<CMaterial>
#else
		public CryModule::Script::IClass,
		public CryModule::Class<CMaterial, _ScriptObject>
#endif
	{
	protected:
		typedef _ScriptObject<CMaterial> TParent;
		typedef SubjectWithID<0> Handler;
		typedef SubjectWithID<1> ColorHandler;
		typedef SubjectWithID<2> TextureHandler;
		typedef SubjectWithID<3> SettingsHandler;
		typedef SubjectWithID<4> LigthingHandler;
		typedef SubjectWithID<5> MapsHandler;
		typedef SubjectWithID<6> VectorHandler;
	public:

		CMaterial();
		virtual ~CMaterial ();

		static void InitializeTemplate(IScriptSystem *pSS);
		static void ReleaseTemplate();
		static IScriptObject* CreateSelf(IFunctionHandler *pH);
		static ::Script::ObjectThunk *GetObjectThunk();

		int AssignMaterial(IFunctionHandler *pH);
		
		int SetTexture(IFunctionHandler *pH);
		int GetTexture(IFunctionHandler *pH);
		
		IScriptObject* GetClassScriptObject();
	protected:

		void Update();

		static void RegisterGlobals(IScriptSystem *pSS);
		static void RegisterProperties();

		void UpdateShaderParams(Subject *pSubject, void *pObject[2], bool bIsPointer);
		
		IMatInfo *m_pMatInfo;
	};
}