#include "Material.h"

#if 1

#include <ShaderUtils.h>
#include <I3DEngine.h>


#define REG_GLOBAL(_value_) pSS->SetGlobalValue(#_value_,(int)_value_)

namespace Editor
{
	namespace Table
	{
		struct STextureMapKey
		{
			const char *m_szKey;
			const char *m_szID;
		}
		s_aTextureMapKey[] = 
		{
			{"Diffuse","0"},
			{"Bump","1"},
			{"Specular","2"},
			{"Detail","3"},
			{"Decal","4"},
			{"Normal","5"},
			{"SCubeMap","6"},
			{"SSpecular","7"},
			{NULL,NULL}
		};

		struct SShaderParamKey
		{
			const char *m_szPrefix;
			PropertyType m_nType;
		} 
		s_aShaderParamKey[] =
		{
			/*eType_UNKNOWN*/{"",PROPERTY_TYPE_OBJECT},
			/*eType_BYTE*/{"c",PROPERTY_TYPE_INT},
			/*eType_BOOL*/{"b",PROPERTY_TYPE_BOOL},
			/*eType_SHORT*/{"s",PROPERTY_TYPE_INT},
			/*eType_INT*/{"n",PROPERTY_TYPE_INT},
			/*eType_FLOAT*/{"f",PROPERTY_TYPE_FLOAT},
			/*eType_STRING*/{"sz",PROPERTY_TYPE_STRING},
			/*eType_FCOLOR*/{"clr",PROPERTY_TYPE_OBJECT},
			/*eType_VECTOR*/{"v",PROPERTY_TYPE_OBJECT}
		};
	}

	CMaterial::CMaterial() : m_pMatInfo(NULL)
	{

	}
	CMaterial::~CMaterial ()
	{
		m_pMatInfo = NULL;
		TParent::Release(false);
	}

	void CMaterial::InitializeTemplate(IScriptSystem *pSS)
	{
		RegisterProperties();

		RegisterGlobals(pSS);
	}

	void CMaterial::ReleaseTemplate()
	{
		ReleaseAll();
	}

	void CMaterial::RegisterGlobals(IScriptSystem *pSS)
	{
		
		REG_GLOBAL(FT_PROJECTED); 
		REG_GLOBAL(FT_NOMIPS);
		REG_GLOBAL(FT_HASALPHA);
		REG_GLOBAL(FT_NORESIZE);
		REG_GLOBAL(FT_HDR);
		REG_GLOBAL(FT_UPDATE);
		REG_GLOBAL(FT_ALLOCATED);
		REG_GLOBAL(FT_BUILD);

		REG_GLOBAL(FT_NODOWNLOAD);
		REG_GLOBAL(FT_CONV_GREY);
		REG_GLOBAL(FT_LM);
		REG_GLOBAL(FT_HASDSDT);

		REG_GLOBAL(FT_HASNORMALMAP);
		REG_GLOBAL(FT_DYNAMIC);
		REG_GLOBAL(FT_NOREMOVE);
		REG_GLOBAL(FT_HASMIPS);
		REG_GLOBAL(FT_PALETTED);
		REG_GLOBAL(FT_NOTFOUND);
		REG_GLOBAL(FT_FONT);
		REG_GLOBAL(FT_SKY);
		REG_GLOBAL(FT_SPEC_MASK);
		REG_GLOBAL(FT_CLAMP);
		REG_GLOBAL(FT_NOSTREAM);
		REG_GLOBAL(FT_DXT1);
		REG_GLOBAL(FT_DXT3);
		REG_GLOBAL(FT_DXT5);
		REG_GLOBAL(FT_DXT);
		REG_GLOBAL(FT_3DC);
		REG_GLOBAL(FT_3DC_A);     
		REG_GLOBAL(FT_ALLOW3DC);  

		REG_GLOBAL(FT_BUMP_SHIFT);
		REG_GLOBAL(FT_BUMP_MASK); 
		REG_GLOBAL(FT_BUMP_DETALPHA);  
		REG_GLOBAL(FT_BUMP_DETRED);    
		REG_GLOBAL(FT_BUMP_DETBLUE);   
		REG_GLOBAL(FT_BUMP_DETINTENS); 

	//////////////////////////////////////////////////////////////////////
		REG_GLOBAL(FT2_NODXT);       
		REG_GLOBAL(FT2_RENDERTARGET); 
		REG_GLOBAL(FT2_FORCECUBEMAP); 
		REG_GLOBAL(FT2_WASLOADED);    
		REG_GLOBAL(FT2_RELOAD);       
		REG_GLOBAL(FT2_NEEDRESTORED); 
		REG_GLOBAL(FT2_UCLAMP);       
		REG_GLOBAL(FT2_VCLAMP);       
		REG_GLOBAL(FT2_RECTANGLE);    
		REG_GLOBAL(FT2_FORCEDXT);     

		REG_GLOBAL(FT2_BUMPHIGHRES);  
		REG_GLOBAL(FT2_BUMPLOWRES);   
		REG_GLOBAL(FT2_PARTIALLYLOADED);  
		REG_GLOBAL(FT2_NEEDTORELOAD);     
		REG_GLOBAL(FT2_WASUNLOADED);      
		REG_GLOBAL(FT2_STREAMINGINPROGRESS);  

		REG_GLOBAL(FT2_FILTER_BILINEAR);  
		REG_GLOBAL(FT2_FILTER_TRILINEAR); 
		REG_GLOBAL(FT2_FILTER_ANISOTROPIC); 
		REG_GLOBAL(FT2_FILTER_NEAREST);
		REG_GLOBAL(FT2_FILTER); 
		REG_GLOBAL(FT2_VERSIONWASCHECKED);  
		REG_GLOBAL(FT2_BUMPCOMPRESED);      
		REG_GLOBAL(FT2_BUMPINVERTED);      
		REG_GLOBAL(FT2_STREAMFROMDDS);      
		REG_GLOBAL(FT2_DISCARDINCACHE);
		REG_GLOBAL(FT2_NOANISO);
		REG_GLOBAL(FT2_CUBEASSINGLETEXTURE);
		REG_GLOBAL(FT2_FORCEMIPS2X2);         
		REG_GLOBAL(FT2_DIFFUSETEXTURE);
		REG_GLOBAL(FT2_WASFOUND);             
		REG_GLOBAL(FT2_REPLICATETOALLSIDES);  
		REG_GLOBAL(FT2_CHECKFORALLSEQUENCES); 

		REG_GLOBAL(eTT_Base);
		REG_GLOBAL(eTT_Cubemap);
		REG_GLOBAL(eTT_AutoCubemap);
		REG_GLOBAL(eTT_Bumpmap);
		REG_GLOBAL(eTT_DSDTBump);
		REG_GLOBAL(eTT_Rectangle);
		REG_GLOBAL(eTT_3D);
		
		REG_GLOBAL(ETMR_NoChange);
		REG_GLOBAL(ETMR_Fixed);
		REG_GLOBAL(ETMR_Constant);
		REG_GLOBAL(ETMR_Oscillated);

		REG_GLOBAL(ETMM_NoChange);
		REG_GLOBAL(ETMM_Fixed);
		REG_GLOBAL(ETMM_Constant);
		REG_GLOBAL(ETMM_Jitter);
		REG_GLOBAL(ETMM_Pan);
		REG_GLOBAL(ETMM_Stretch);
		REG_GLOBAL(ETMM_StretchRepeat);

		REG_GLOBAL(ETG_Stream);
		REG_GLOBAL(ETG_World);
		REG_GLOBAL(ETG_Camera);
		REG_GLOBAL(ETG_WorldEnvMap);
		REG_GLOBAL(ETG_CameraEnvMap);
		REG_GLOBAL(ETG_NormalMap);
		REG_GLOBAL(ETG_SphereMap);

		REG_GLOBAL(eGTC_NoFill);
		REG_GLOBAL(eGTC_None);
		REG_GLOBAL(eGTC_LightMap);
		REG_GLOBAL(eGTC_Quad);
		REG_GLOBAL(eGTC_Base);
		REG_GLOBAL(eGTC_Projection);
		REG_GLOBAL(eGTC_Environment);
		REG_GLOBAL(eGTC_SphereMap);
		REG_GLOBAL(eGTC_SphereMapEnvironment);
		REG_GLOBAL(eGTC_ShadowMap);

	}




	//////////////////////////////////////////////////////////////////////

	void CMaterial::RegisterProperties()
	{
		Context::Init();

		Subject *pRoot =			Handler::				Create()->Register(offsetof(CMatInfo, shaderItem.m_pShaderResources));
		Subject *pColor =			ColorHandler::			Create();
		Subject *pVector =			VectorHandler::			Create();
		Subject *pTexture =			TextureHandler::		Create();

		//Subject *pRoot =			Handler::				Create();
		Subject *pSettings =		SettingsHandler::		Create();
		Subject *pLigthing =		LigthingHandler::		Create();
		Subject *pMaps =			MapsHandler::			Create();


		SubjectAs<SRenderShaderResources>(pRoot)
			.RegisterMethod(&CMaterial::AssignMaterial, "AssignMaterial")
			.Subject(pMaps)
				.Register(&SRenderShaderResources::m_Textures, "Maps")
			.SubClass<SBaseShaderResources>(1)
				.Subject(&CMaterial::UpdateShaderParams)
					.Register(&SBaseShaderResources::m_ShaderParams, "ShaderParams")
				.Subject(pSettings)
					.RegisterAs("Settings")
				.Subject(pLigthing)
					.Register(&SBaseShaderResources::m_LMaterial, "Ligthing")
		;

		SubjectAs<SRenderShaderResources>(pSettings)
			.SubClass<SBaseShaderResources>(1).Prefix(true)
				.Register(&SBaseShaderResources::m_Opacity, "Opacity")
				.Register(&SBaseShaderResources::m_AlphaRef, "AlphaTest")
		;
		
		SubjectAs<CFColor>(pColor, "clr")
			.Register(&CFColor::r, "r")
			.Register(&CFColor::g, "g")
			.Register(&CFColor::b, "b")
			.Register(&CFColor::a, "a")
		;

		SubjectAs<Vec3>(pVector, "v")
			.Register(&Vec3::x, "x")
			.Register(&Vec3::y, "y")
			.Register(&Vec3::z, "z")
		;

		SubjectAs<SLightMaterial>(pLigthing)
			.Create(&SLightMaterial::Front).Prefix(true)
				.Register(&SSideMaterial::m_SpecShininess,"Shininess")
				.Subject(pColor)
					.Register(&SSideMaterial::m_Ambient,"Ambient")
					.Register(&SSideMaterial::m_Diffuse,"Diffuse")
					.Register(&SSideMaterial::m_Specular,"Specular")
					.Register(&SSideMaterial::m_Emission,"Emission")
		;

		{
			SubjectAs<SEfResTexture> aSubject(pTexture); aSubject.Prefix(true)
				.RegisterMethod(&CMaterial::GetTexture, "GetTexture")
				.RegisterMethod(&CMaterial::SetTexture, "SetTexture")
				.Register(&SEfResTexture::m_bUTile, "IsTileU")
				.Register(&SEfResTexture::m_bVTile, "IsTileV")
			;
			{
				aSubject.Create(&SEfResTexture::m_TU).Prefix(true)
					.Register(&SShaderTexUnit::m_eTexType, "Type")
					.Register(&SShaderTexUnit::m_eGenTC,"GenType")
				;
			}
			{
				aSubject.Create(&SEfResTexture::m_TexModificator).Prefix(true)
					.Register(&SEfTexModificator::m_eRotType, "RotatorType")
					.Register(&SEfTexModificator::m_eUMoveType, "OscillatorTypeU")
					.Register(&SEfTexModificator::m_eVMoveType, "OscillatorTypeV")
					.Register(&SEfTexModificator::m_bTexGenProjected, "IsGenProjected")
					.Postfix("U", "V", "W", NULL)
						.Register(&SEfTexModificator::m_Tiling,"Tile")
						.Register(&SEfTexModificator::m_Offs,"Offset")
						.Register(&SEfTexModificator::m_Rot,"Rotate")
						.Register(&SEfTexModificator::m_RotOscRate,"Rate")
						.Register(&SEfTexModificator::m_RotOscPhase,"Phase")
						.Register(&SEfTexModificator::m_RotOscAmplitude,"Amplitude")
						.Register(&SEfTexModificator::m_RotOscCenter,"Center")
					.Postfix(NULL)
					.Register(&SEfTexModificator::m_UOscRate,"OscillatorRateU")
					.Register(&SEfTexModificator::m_VOscRate,"OscillatorRateV")
					.Register(&SEfTexModificator::m_UOscPhase,"OscillatorPhaseU")
					.Register(&SEfTexModificator::m_VOscPhase,"OscillatorPhaseV")
					.Register(&SEfTexModificator::m_UOscAmplitude,"OscillatorAmplitudeU")
					.Register(&SEfTexModificator::m_VOscAmplitude,"OscillatorAmplitudeV")
				;
			}
		}
		{
			size_t nOffset, nIndex = 0;

			for (Table::STextureMapKey *pKey = &Table::s_aTextureMapKey[0]; pKey->m_szKey; pKey++)
			{
				nOffset = Shader::GetTextureType(nIndex++) * sizeof(SEfResTexture*);

				pMaps->RegisterSubject( pKey->m_szKey, nOffset, pTexture);
				pMaps->RegisterSubject( pKey->m_szID, nOffset, pTexture);
			}
		}

		Context::GetPipeline()->Defragmentate();
	}
	int CMaterial::GetTexture(IFunctionHandler *pH)
	{
		if (!Context::CheckCount(2))
		{
			return pH->EndFunction(-2);
		}
		
		SEfResTexture *pResTexture = Context::Back<SEfResTexture>();

		if (!pResTexture)
		{
			return pH->EndFunction(-3);
		}

		if (!(pResTexture->m_TU.m_ITexPic))
		{
			return pH->EndFunction(-4);
		}

		return pH->EndFunction(pResTexture->m_TU.m_ITexPic->GetTextureID());
	}

	int CMaterial::SetTexture(IFunctionHandler *pH)
	{
		if (!Context::CheckCount(2))
		{
			return pH->EndFunction(-1);
		}
		
		SEfResTexture *pResTexture = Context::Back<SEfResTexture>();

		if (!pResTexture)
		{
			return pH->EndFunction(-2);
		}

		if (!(pResTexture->m_TU.m_ITexPic))
		{
			return pH->EndFunction(-3);
		}

		if (pH->GetParamCount() == 0)
		{
			return pH->EndFunction(-4);
		}

		ITexPic *pTexPic = NULL;

		switch(pH->GetParamType(1))
		{
		case svtString: {
				const char *szFilePath;

				int flags = FT_NOREMOVE, 
					flags2 = 0, 
					eTT = eTT_Base, count = pH->GetParamCount();

				pH->GetParam(1, szFilePath);

				if (count > 1)
				{
					pH->GetParam(2, flags);
				}

				if (count > 2)
				{
					pH->GetParam(3, flags2);
				}

				if (count > 3)
				{
					pH->GetParam(4, eTT);
				}


				pTexPic = CryModule::aSystem.Get<IRenderer>()->EF_LoadTexture(szFilePath, (uint)flags, (uint)flags, (byte)eTT);

			} break;

		case svtNumber: {
				int tid;

				pH->GetParam(1, tid);

				pTexPic =CryModule::aSystem.Get<IRenderer>()->EF_GetTextureByID(tid);

			} break;

		case svtUserData: {
				
				int iCookie = 0;
				INT_PTR tid;

				pH->GetParamUDVal(1,tid, iCookie);
				
				pTexPic = CryModule::aSystem.Get<IRenderer>()->EF_GetTextureByID(tid);
			} break;
		}

		
		pResTexture->m_TU.m_ITexPic = pTexPic;

		return pH->EndFunction(pTexPic ? pTexPic->GetTextureID() : -5);
	}

	int CMaterial::AssignMaterial(IFunctionHandler *pH)
	{		
		int nMaterialSlot;
		{
			m_pMatInfo = NULL;
			Clear();
		}	
		switch(pH->GetParamType(1))
		{
		case svtNumber: {
				
				int nEntityId;
				pH->GetParam(1, nEntityId);
				IEntity *pEntity = CryModule::aSystem.Get<IEntitySystem>()->GetEntity(nEntityId);
				if (!pEntity)
					return pH->EndFunctionNull();
				
				int nObjectSlot = 0;

				pH->GetParam(2, nObjectSlot);
				pH->GetParam(3, nMaterialSlot);

				m_pMatInfo = Shader::GetMaterialFromEntity(pEntity, nObjectSlot, nMaterialSlot);

			} break;

		case svtString: {
				const char *szMaterial = NULL;

				pH->GetParam(1, szMaterial);

				if (!szMaterial)
				{
					return pH->EndFunctionNull();
				}

				
				m_pMatInfo = (CMatInfo*)CryModule::aSystem.Get<I3DEngine>()->FindMaterial( szMaterial );

				if (m_pMatInfo && pH->GetParamCount() == 2)
				{
					pH->GetParam(2, nMaterialSlot);
					
					if (nMaterialSlot > 0)
					{
						if (--nMaterialSlot < m_pMatInfo->GetSubMtlCount())
						{
							m_pMatInfo = m_pMatInfo->GetSubMtl(nMaterialSlot);
						}
						else
						{
							m_pMatInfo = NULL;
						}
					}
				}
				
			} break;
		}
		
		if (m_pMatInfo != NULL)
		{
			EnableMapping(m_pMatInfo);
		}
		
		return pH->EndFunction(1);
	}

	void CMaterial::UpdateShaderParams(Subject *pSubject, void *pObject[2], bool bIsPointer)
	{
		SShaderParam *pParams = (SShaderParam *)pObject[1];
		size_t nParams = *((size_t*)pObject[0] + 1);
		SShaderParam *p;
		Table::SShaderParamKey *pKey;


		for (size_t i = 0, c = nParams, nOffset; i < c; i++)
		{
			p = &pParams[i];
			pKey = &Table::s_aShaderParamKey[p->m_Type];

			string szName = p->m_Name;
			
			{
				szName[0] = toupper(szName [0]);
				szName = pKey->m_szPrefix + szName;//combination of type prefix with name
			}
			{
				nOffset = i * sizeof(SShaderParam) + offsetof(SShaderParam, m_Value);
			}

			if (pKey->m_nType == PROPERTY_TYPE_OBJECT)
			{
				if (p->m_Type == eType_VECTOR)
				{ 
					pSubject->RegisterSubject(szName.c_str(), nOffset, VectorHandler::I(), false);
				}
				else if (p->m_Type == eType_FCOLOR)
				{
					pSubject->RegisterSubject(szName.c_str(), nOffset, ColorHandler::I(), false);
				}
			}
			else
			{
				pSubject->Register(szName.c_str(), pKey->m_nType, nOffset );
			}
		}
	}

	IScriptObject* CMaterial::CreateSelf(IFunctionHandler *pH)
	{
		return Utils::Plugin::Class::aBundle.Register<CMaterial>(false, CreateScriptObject(pH, NULL))->GetClassScriptObject();
	}

	::Script::ObjectThunk *CMaterial::GetObjectThunk()
	{
		static ::Script::ObjectThunk aThunk = {false, &CMaterial::InitializeTemplate, &CMaterial::ReleaseTemplate, &CMaterial::CreateSelf, NULL};

		return &aThunk;
	}

	
	IScriptObject* CMaterial::GetClassScriptObject()
	{
		return m_SO[0];
	}

	
}




#endif